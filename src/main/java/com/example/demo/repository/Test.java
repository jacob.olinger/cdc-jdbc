package com.example.demo.repository;

import com.example.demo.entity.Products;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

    public static void main(String[] args) {
        ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
        ProductsDao dao=(ProductsDao)ctx.getBean("edao");
        int statusCre=dao.createProducts();
        System.out.println(statusCre);

        int status=dao.saveProducts(new Products(102, 35000.0, "Amit"));
        System.out.println(status);


    int statusUp=dao.updateProducts(new Products(102,15000.0,"Sonoo"));
    System.out.println(statusUp);


    Products e=new Products();
    e.setProductId(102);
    int statusDel=dao.deleteProducts(e);
    System.out.println(statusDel);

    }

}

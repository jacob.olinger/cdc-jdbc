package com.example.demo.repository;

import com.example.demo.entity.Products;
import org.springframework.jdbc.core.JdbcTemplate;

public class ProductsDao {
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int createProducts(){
        String query ="CREATE TABLE if not exists product_Val (productId INT,price FLOAT,sku VARCHAR(255),PRIMARY KEY(productId))";
        jdbcTemplate.execute(query);
        return 1;
    }
    public int saveProducts(Products e){
        String query="insert into product_Val values("+e.getProductId()+","+e.getPrice()+",'"+e.getSku()+"')";
        return jdbcTemplate.update(query);

    }
    public int updateProducts(Products e){
        String query="update productVal set productId="+e.getProductId()+",price="+e.getPrice()+" where sku='"+e.getSku()+"' ";
        return jdbcTemplate.update(query);
    }
    public int deleteProducts(Products e){
        String query="delete from productVal where productId="+e.getProductId()+" ";
        return jdbcTemplate.update(query);
    }

}

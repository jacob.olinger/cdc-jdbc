package com.example.demo.repository;
import com.example.demo.entity.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Product extends JpaRepository<Products, Integer> {

    public boolean existsById(String id);

    public List<Products> findByEmail(String email);

    @Query("select max(s.id) from Products s")
    public Integer findMaxId();
}

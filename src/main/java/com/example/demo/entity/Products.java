package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Products {

    @Id
    private int productId;

    private double price;

    private String sku;

    public Products() {
    }

    public Products(int id, double p, String s) {
        this.productId=id;
        this.price=p;
        this.sku=s;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @Override
    public String toString() {
        return "Products{" +
                "productId=" + productId +
                ", price='" + price + '\'' +
                ", sku='" + sku + '\'' +
                '}';
    }
}
